import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formularioreact',
  templateUrl: './formularioreact.component.html',
  styleUrls: ['./formularioreact.component.css']
})
export class FormularioreactComponent implements OnInit {

  guardados:string[]=[];
  formulario!:FormGroup;
  constructor(private fb:FormBuilder) { 
    this.crearForm();
  }

 
  get getInputs(){
    return this.formulario.get('input1') as FormArray;
  }


  ngOnInit(): void {
  }

  crearForm(){
    this.formulario = this.fb.group({
      input1:this.fb.array([]), 
    })

    this.getInputs.push(
      this.fb.group({
      textoNuevo : ['',[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]]
    }))
  }
  //Guarda valores

  guardar():void{
  
    for (let i = 0; i < this.getInputs.length; i++) {
      
    if (this.getInputs.at(i).get('textoNuevo')?.valid) {
      if(this.getInputs.at(i).get('textoNuevo')?.value !== null || ! (this.getInputs.at(i).get('textoNuevo')?.value === '')){
        this.guardados.push(this.getInputs.at(i).get('textoNuevo')?.value);
     }
    }
    }
  
  }
  //limpiar individual por caja

  limpiar(id:number){
    for (let i = 0; i < this.getInputs.length; i++) {
      if (id===i) {
        this.getInputs.at(i).get('textoNuevo')?.setValue('');
      }
    }
  }
//Eliminar indi
  indiEli(id:number):void{
    this.getInputs.removeAt(id)
  }

  listalim():void{
    this.guardados = [''];
    this.formulario.reset;
  }
//Boton de limpiar todo
limpiarTodo():void{
    this.guardados=[''];
  }
  //Aumentar datos

  adicionar():void{
    const nuevo = this.fb.group({
      textoNuevo : ['',Validators.pattern(/^[a-zA-zñÑ\s]+$/)]
    })
    this.getInputs.push(nuevo);
  }
//Caja de mostrar
  mostrar():string{
    return this.guardados.join("\n");
 }



}
