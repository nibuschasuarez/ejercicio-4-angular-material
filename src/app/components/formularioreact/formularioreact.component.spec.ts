import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioreactComponent } from './formularioreact.component';

describe('FormularioreactComponent', () => {
  let component: FormularioreactComponent;
  let fixture: ComponentFixture<FormularioreactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioreactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioreactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
